{ pkgs ? import (fetchTarball {
  url = "https://github.com/NixOS/nixpkgs/archive/nixos-24.05.tar.gz";
  sha256 = "1q7y5ygr805l5axcjhn0rn3wj8zrwbrr0c6a8xd981zh8iccmx0p";
}) { } }:
pkgs.mkShell { buildInputs = with pkgs; [ nodejs_22 ]; }
