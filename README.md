# Temple of Terror

[![gitlab repo](https://img.shields.io/badge/gitlab-fucod%2Ftempel--des--schreckens-blue)](https://gitlab.com/fucod/tempel-des-schreckens)
[![app website](https://img.shields.io/badge/fucod.com-tds-blue)](https://tds.fucod.com)

Classic board game 'Tempel des Schreckens' web version

# About

This game is a web version of the classic board game 'Tempel des Schreckens', the original board game designer is": **佐藤 雄介 (Yusuke Sato)**

If you find this game fun and have gained a lot of enjoyment from it, please consider purchasing the physical card version of the board game, it will bring a completely new gaming experience.

- Official Website: https://www.schmidtspiele.de/details/produkt/tempel-des-schreckens.html
- BGG - Tempel des Schreckens: https://boardgamegeek.com/boardgame/206915/tempel-des-schreckens

This game is an open-source project, any comments and feedback are welcome.

- GitLab Repo: https://gitlab.com/fucod/tempel-des-schreckens
- App Website: https://tds.fucod.com

# Development

## Base Workspace

This project is based on **base-workspace** , please references `BW_README.md` for details.

## Prerequisites

- Nix

## Prepare development environment

1. Run `nix-shell` to setup node environment.
2. In nix-shell, run `npm install` to install npm package dependency.

## Run the application dev server

    npx nx serve tpds-app

# Copyright

Copyright (c) 2024 Future Code. Released under the GPLv3 license.
