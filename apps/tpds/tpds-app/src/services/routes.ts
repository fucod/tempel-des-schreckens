import EntryPage from '../pages/Entry.vue';
import TempleEntryPage from '../pages/temple/Entry.vue';
import TempleConnectionPage from '../pages/temple/Connection.vue';
import TempleRoomPage from '../pages/temple/Room.vue';
import TempleStartPage from '../pages/temple/Start.vue';
import TempleRoundPage from '../pages/temple/Round.vue';
import TempleGameOverPage from '../pages/temple/GameOver.vue';
import PlayerEntryPage from '../pages/player/Entry.vue';
import PlayerConnectionPage from '../pages/player/Connection.vue';
import PlayerWaitingPage from '../pages/player/Waiting.vue';
import PlayerInfoPage from '../pages/player/Info.vue';
import PlayerRoomPage from '../pages/player/Room.vue';
import PlayerRulePage from '../pages/player/Rule.vue';
import PlayerGameOverPage from '../pages/player/GameOver.vue';
import RulePage from '../pages/Rule.vue';
import ErrorPage from '../pages/Error.vue';
import AboutPage from '../pages/About.vue';
import DevPage from '../pages/Dev.vue';
import SettingsPage from '../pages/Settings.vue';

export const routes = [
  {
    path: '/',
    name: 'entry',
    component: EntryPage
  },
  {
    path: '/temple/entry',
    name: 'temple_entry',
    component: TempleEntryPage
  },
  {
    path: '/temple/connection',
    name: 'temple_connection',
    component: TempleConnectionPage
  },
  {
    path: '/temple/start',
    name: 'temple_start',
    component: TempleStartPage
  },
  {
    path: '/temple/round',
    name: 'temple_round',
    component: TempleRoundPage
  },
  {
    path: '/temple/room',
    name: 'temple_room',
    component: TempleRoomPage
  },
  {
    path: '/temple/gameover',
    name: 'temple_gameover',
    component: TempleGameOverPage
  },
  {
    path: '/player/entry',
    name: 'player_entry',
    component: PlayerEntryPage
  },
  {
    path: '/player/connection',
    name: 'player_connection',
    component: PlayerConnectionPage
  },
  {
    path: '/player/waiting',
    name: 'player_waiting',
    component: PlayerWaitingPage
  },
  {
    path: '/player/info',
    name: 'player_info',
    component: PlayerInfoPage
  },
  {
    path: '/player/room',
    name: 'player_room',
    component: PlayerRoomPage
  },
  {
    path: '/player/rule',
    name: 'player_rule',
    component: PlayerRulePage
  },
  {
    path: '/player/gameover',
    name: 'player_gameover',
    component: PlayerGameOverPage
  },
  {
    path: '/rule',
    name: 'rule',
    component: RulePage
  },
  {
    path: '/about',
    name: 'about',
    component: AboutPage
  },
  {
    path: '/settings',
    name: 'settings',
    component: SettingsPage
  },
  {
    path: '/dev',
    name: 'dev',
    component: DevPage
  },
  {
    path: '/error',
    name: 'error',
    component: ErrorPage
  },
  {
    path: '/:pathMatch(.*)*',
    component: EntryPage
  }
];
