import { Uid } from '@base-workspace/bw-model';
import { IconifyIcon } from '@iconify/vue';
import GameIconsRupee from '@iconify-icons/game-icons/rupee';
import GameIconsCelebrationFire from '@iconify-icons/game-icons/celebration-fire';
import GameIconsStoneWall from '@iconify-icons/game-icons/stone-wall';
import GameIconsWoodenDoor from '@iconify-icons/game-icons/wooden-door';
import GameIconsDigDug from '@iconify-icons/game-icons/dig-dug';
import GameIconsTribalShield from '@iconify-icons/game-icons/tribal-shield';

export type PlayerRole = 'adventurer' | 'guardian';
export type DoorKind = 'unknown' | 'gold' | 'fire' | 'empty';
export type GameStatus =
  | 'init'
  | 'register'
  | 'playing'
  | 'next-round'
  | 'gameover';
export type PlayerRoleMap = Record<Uid, PlayerRole>;
export type PlayerDoorMap = Record<Uid, DoorKind[]>;

type RoleCount = {
  adventurer: number;
  guardian: number;
};

type DoorCount = {
  found: number;
  total: number;
};

type DoorKindCount = {
  gold: number;
  fire: number;
  empty: number;
};

export type PlayersInfo = {
  uids: Uid[];
  nameMap: Record<Uid, string>;
  peerIdMap: Record<Uid, string>;
};

export type GamePublicInfo = {
  status: GameStatus;
  round: number;
  openedDoorCount: number;
  playerOpenDoorMap: PlayerDoorMap;
  keyholderUid?: Uid;
  role: RoleCount;
  gold: DoorCount;
  fire: DoorCount;
  empty: DoorCount;
  lastDoorOpen?: DoorChoice;
};

export type GameState = GamePublicInfo & {
  playerRoleMap: PlayerRoleMap;
  playerHoldingDoorMap: PlayerDoorMap;
};

export type GameOverInfo = {
  winner: PlayerRole;
  reason: 'gold' | 'fire' | 'timeout';
};

export type DoorChoice = {
  uid: Uid;
  num: number;
};

export function dispatchPlayerRole(uids: Uid[]): PlayerRoleMap {
  const roleCount = generateRoleCount(uids.length);
  const prMap: PlayerRoleMap = {};
  const roles: PlayerRole[] = genShuffledOptions(roleCount) as PlayerRole[];
  for (let i = 0; i < uids.length; i++) {
    prMap[uids[i]] = roles[i];
  }
  return prMap;
}

export function generateRoleCount(players: number): RoleCount {
  switch (players) {
    case 3:
      return { adventurer: 2, guardian: 2 };
    case 4:
      return { adventurer: 3, guardian: 2 };
    case 5:
      return { adventurer: 3, guardian: 2 };
    case 6:
      return { adventurer: 4, guardian: 2 };
    case 7:
      return { adventurer: 5, guardian: 3 };
    case 8:
      return { adventurer: 6, guardian: 3 };
    case 9:
      return { adventurer: 6, guardian: 3 };
    case 10:
    default:
      return { adventurer: 7, guardian: 4 };
  }
}

export function generateDoorCount(players: number): DoorKindCount {
  switch (players) {
    case 3:
      return { empty: 8, gold: 5, fire: 2 };
    case 4:
      return { empty: 12, gold: 6, fire: 2 };
    case 5:
      return { empty: 16, gold: 7, fire: 2 };
    case 6:
      return { empty: 20, gold: 8, fire: 2 };
    case 7:
      return { empty: 26, gold: 7, fire: 2 };
    case 8:
      return { empty: 30, gold: 8, fire: 2 };
    case 9:
      return { empty: 34, gold: 9, fire: 2 };
    case 10:
    default:
      return { empty: 37, gold: 10, fire: 3 };
  }
}

export function dispatchPlayerDoors(
  round: number,
  uids: Uid[],
  doorCount: DoorKindCount
): { open: PlayerDoorMap; holding: PlayerDoorMap } {
  const openPdMap: PlayerDoorMap = {};
  const holdingPdMap: PlayerDoorMap = {};
  const numDoorsToDispatch = 5 - round + 1;
  const doors: DoorKind[] = genShuffledOptions(doorCount) as DoorKind[];
  for (let i = 0; i < uids.length; i++) {
    openPdMap[uids[i]] = new Array(numDoorsToDispatch).fill('unknown');
    holdingPdMap[uids[i]] = doors.slice(
      i * numDoorsToDispatch,
      (i + 1) * numDoorsToDispatch
    );
  }
  return { open: openPdMap, holding: holdingPdMap };
}

function genShuffledOptions(countMap: Record<string, number>): string[] {
  const options: string[] = [];
  for (const k of Object.keys(countMap)) {
    for (let i = 0; i < countMap[k]; i++) {
      options.push(k);
    }
  }
  for (let i = options.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [options[i], options[j]] = [options[j], options[i]];
  }
  return options;
}

export function buildInitGameState(): GameState {
  return {
    status: 'init',
    round: 0,
    openedDoorCount: 0,
    playerOpenDoorMap: {},
    role: {
      adventurer: 0,
      guardian: 0
    },
    gold: {
      found: 0,
      total: 0
    },
    fire: {
      found: 0,
      total: 0
    },
    empty: {
      found: 0,
      total: 0
    },
    playerRoleMap: {},
    playerHoldingDoorMap: {}
  };
}

export function doorKindIcon(doorKind: DoorKind): IconifyIcon {
  switch (doorKind) {
    case 'unknown':
      return GameIconsWoodenDoor;
    case 'gold':
      return GameIconsRupee;
    case 'empty':
      return GameIconsStoneWall;
    case 'fire':
      return GameIconsCelebrationFire;
  }
}

export function roleIcon(role: PlayerRole): IconifyIcon {
  switch (role) {
    case 'adventurer':
      return GameIconsDigDug;
    case 'guardian':
      return GameIconsTribalShield;
  }
}

export function openedDoorCountInRound(
  players: number,
  publicInfo: GamePublicInfo
): number {
  if (publicInfo.round === 0) {
    return 0;
  }
  return (
    publicInfo.gold.found +
    publicInfo.fire.found +
    publicInfo.empty.found -
    players * (publicInfo.round - 1)
  );
}
