import * as PageGuide from '@base-workspace/bw-page-guide';
import GameIconsEgyptianTemple from '@iconify-icons/game-icons/egyptian-temple';
import GameIconsDungeonGate from '@iconify-icons/game-icons/dungeon-gate';
import GameIconsPikeman from '@iconify-icons/game-icons/pikeman';
import mdiLoginVariant from '@iconify-icons/mdi/login-variant';
import GameIconsWaxTablet from '@iconify-icons/game-icons/wax-tablet';
import mdiCards from '@iconify-icons/mdi/cards';
import mdiTempleHindu from '@iconify-icons/mdi/temple-hindu';
import mdiStar from '@iconify-icons/mdi/star';
import mdiCog from '@iconify-icons/mdi/cog';
import mdiTools from '@iconify-icons/mdi/tools';
import mdiHome from '@iconify-icons/mdi/home';

export const PAGES: PageGuide.PageGuideData[] = [
  {
    code: 'temple',
    route: { name: 'temple_entry' },
    nameLk: 'pages.temple.entry.title',
    iconInfo: {
      icon: GameIconsEgyptianTemple
    }
  },
  {
    code: 'temple-connection',
    parent: 'temple',
    route: { name: 'temple_connection' },
    nameLk: 'pages.temple.connection.title',
    iconInfo: {
      icon: GameIconsDungeonGate
    }
  },
  {
    code: 'temple-start',
    parent: 'temple',
    route: { name: 'temple_start' },
    nameLk: 'pages.temple.start.title',
    iconInfo: {
      icon: GameIconsDungeonGate
    }
  },
  {
    code: 'player',
    route: { name: 'player_entry' },
    nameLk: 'pages.player.entry.title',
    iconInfo: {
      icon: GameIconsPikeman
    }
  },
  {
    code: 'player-connection',
    parent: 'player',
    route: { name: 'player_connection' },
    nameLk: 'pages.player.connection.title',
    iconInfo: {
      icon: mdiLoginVariant
    }
  },
  {
    code: 'player-waiting',
    parent: 'player',
    route: { name: 'player_waiting' },
    nameLk: 'pages.player.waiting.title',
    iconInfo: {
      icon: mdiLoginVariant
    }
  },
  {
    code: 'player-rule',
    parent: 'player',
    route: { name: 'player_rule' },
    nameLk: 'pages.player.rule.title',
    iconInfo: {
      icon: GameIconsWaxTablet
    }
  },
  {
    code: 'player-info',
    parent: 'player',
    route: { name: 'player_info' },
    nameLk: 'pages.player.info.title',
    iconInfo: {
      icon: mdiCards
    }
  },
  {
    code: 'player-room',
    parent: 'player',
    route: { name: 'player_room' },
    nameLk: 'pages.player.room.title',
    iconInfo: {
      icon: mdiTempleHindu
    }
  },
  {
    code: 'rule',
    route: { name: 'rule' },
    nameLk: 'pages.RulePage.title',
    iconInfo: {
      icon: GameIconsWaxTablet
    }
  },
  {
    code: 'about',
    route: { name: 'about' },
    nameLk: 'pages.AboutPage.title',
    iconInfo: {
      icon: mdiStar
    }
  },
  {
    code: 'settings',
    route: { name: 'settings' },
    nameLk: 'pages.settings.title',
    iconInfo: {
      icon: mdiCog
    }
  },
  {
    code: 'dev',
    route: { name: 'dev' },
    nameLk: 'pages.dev.title',
    iconInfo: {
      icon: mdiTools
    }
  },
  {
    code: 'entry',
    route: { name: 'entry' },
    nameLk: 'app.service.home',
    iconInfo: {
      icon: mdiHome
    }
  }
];
