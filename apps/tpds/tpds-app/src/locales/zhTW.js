import app from './app/zh-TW.json' assert { type: 'json' };
import layouts from './layouts/zh-TW.json' assert { type: 'json' };
import pages from './pages/zh-TW.json' assert { type: 'json' };
import components from './components/zh-TW.json' assert { type: 'json' };
import game from './game/zh-TW.json' assert { type: 'json' };

export default {
  app,
  layouts,
  pages,
  components,
  game
};
