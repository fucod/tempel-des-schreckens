import app from './app/zh-CN.json' assert { type: 'json' };
import layouts from './layouts/zh-CN.json' assert { type: 'json' };
import pages from './pages/zh-CN.json' assert { type: 'json' };
import components from './components/zh-CN.json' assert { type: 'json' };
import game from './game/zh-CN.json' assert { type: 'json' };

export default {
  app,
  layouts,
  pages,
  components,
  game
};
