import app from './app/en-US.json' assert { type: 'json' };
import layouts from './layouts/en-US.json' assert { type: 'json' };
import pages from './pages/en-US.json' assert { type: 'json' };
import components from './components/en-US.json' assert { type: 'json' };
import game from './game/en-US.json' assert { type: 'json' };

export default {
  app,
  layouts,
  pages,
  components,
  game
};
