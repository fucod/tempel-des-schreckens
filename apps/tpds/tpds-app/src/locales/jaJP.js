import app from './app/ja-JP.json' assert { type: 'json' };
import layouts from './layouts/ja-JP.json' assert { type: 'json' };
import pages from './pages/ja-JP.json' assert { type: 'json' };
import components from './components/ja-JP.json' assert { type: 'json' };
import game from './game/ja-JP.json' assert { type: 'json' };

export default {
  app,
  layouts,
  pages,
  components,
  game
};
