import { ref } from 'vue';
import { defineStore } from 'pinia';
import { Room, joinRoom, selfId } from 'trystero';
import * as RandomToken from '@sibevin/random-token';
import appStatic from '../static/app.json';
import { Uid } from '@base-workspace/bw-model';
import {
  DoorChoice,
  DoorKind,
  GameOverInfo,
  GameState,
  PlayerRole,
  PlayersInfo,
  buildInitGameState,
  dispatchPlayerDoors,
  dispatchPlayerRole,
  generateDoorCount,
  openedDoorCountInRound
} from '../services/game';

type PeerStatus = 'none' | 'ready' | 'connected';

type PeerId = string;

type PeerRegisterMsg = {
  name: string;
  uid: string;
};

type PeerError = {
  action: string;
  code: string;
};

export const useTempleStore = defineStore('tpds-temple-store', () => {
  const code = ref<string | null>(null);
  const room = ref<Room | null>(null);
  const isConnected = ref<boolean>(false);
  const error = ref<PeerError | null>(null);
  const gameState = ref<GameState>(buildInitGameState());
  const gameOverInfo = ref<GameOverInfo | null>(null);
  const gameOverResult = ref<{ name: string; role: PlayerRole }[]>([]);
  const playersInfo = ref<PlayersInfo>({
    uids: [],
    nameMap: {},
    peerIdMap: {}
  });

  function openArena() {
    if (room.value) {
      return;
    }
    resetTemple();
    code.value = RandomToken.gen({ length: 4, seed: 'n' }) as string;
    room.value = joinRoom(
      { appId: `${appStatic.appId}_${appStatic.version}` },
      code.value
    );
    gameState.value.status = 'register';
    const [sendArenaError] = room.value.makeAction('arenaError');
    const [sendDone] = room.value.makeAction('done');
    const [, getRegister] = room.value.makeAction('register');
    const [, getDoorOpen] = room.value.makeAction('doorOpen');
    const [, getDisconnect] = room.value.makeAction('disconnect');
    getRegister((msg, peerId) => {
      const { name, uid } = msg as PeerRegisterMsg;
      if (
        gameState.value.status === 'register' ||
        gameState.value.status === 'gameover'
      ) {
        playersInfo.value.uids.push(uid);
        playersInfo.value.nameMap[uid] = name;
        playersInfo.value.peerIdMap[uid] = peerId;
      }
    });
    getDoorOpen((msg, peerId) => {
      const dc = msg as DoorChoice;
      const dcIndex = dc.num - 1;
      const openDoor = gameState.value.playerHoldingDoorMap[dc.uid][dcIndex];
      if (openDoor !== 'unknown') {
        sendArenaError(
          { action: 'doorChoice', code: 'door_already_opened' },
          peerId
        );
      }
      const holdingDoor = gameState.value.playerHoldingDoorMap[dc.uid][
        dcIndex
      ] as DoorKind;
      gameState.value.playerOpenDoorMap[dc.uid][dcIndex] = holdingDoor;
      switch (holdingDoor) {
        case 'gold':
          gameState.value.gold.found += 1;
          break;
        case 'fire':
          gameState.value.fire.found += 1;
          break;
        case 'empty':
          gameState.value.empty.found += 1;
          break;
      }
      if (checkGameOver()) {
        return;
      }
      checkRoundEnd();
      gameState.value.lastDoorOpen = dc;
      gameState.value.keyholderUid = dc.uid;
      updatePublicInfo();
    });
    getDisconnect((msg) => {
      const uid = msg as Uid;
      playersInfo.value.uids.push(uid);
      playersInfo.value.uids = playersInfo.value.uids.filter(
        (pUid) => pUid !== uid
      );
      delete playersInfo.value.nameMap[uid];
      delete playersInfo.value.peerIdMap[uid];
      updatePublicInfo();
    });
    room.value.onPeerJoin((peerId) => {
      isConnected.value = true;
      sendDone({ action: 'connect' }, peerId);
      updatePublicInfo();
    });
    room.value.onPeerLeave(() => {
      if (room.value && Object.keys(room.value.getPeers()).length === 0) {
        isConnected.value = false;
      }
    });
  }

  async function closeArena() {
    if (!room.value) {
      return;
    }
    const [sendArenaClose] = room.value.makeAction('arenaClose');
    sendArenaClose(null);
    room.value.leave();
    room.value = null;
    playersInfo.value = {
      uids: [],
      nameMap: {},
      peerIdMap: {}
    };
  }

  async function setupGame() {
    if (!room.value) {
      return;
    }
    resetTemple();
    gameState.value.status = 'playing';
    const [sendPlayersInfo] = room.value.makeAction('playersInfo');
    sendPlayersInfo(playersInfo.value);
    const uids = playersInfo.value.uids;
    const prMap = dispatchPlayerRole(uids);
    gameState.value.playerRoleMap = prMap;
    gameState.value.role.adventurer = Object.values(prMap).filter(
      (role) => role === 'adventurer'
    ).length;
    gameState.value.role.guardian = Object.values(prMap).filter(
      (role) => role === 'guardian'
    ).length;
    const [sendRoleDispatch] = room.value.makeAction('roleDispatch');
    for (const uid of uids) {
      const role = prMap[uid];
      sendRoleDispatch(role, playersInfo.value.peerIdMap[uid]);
    }
    const doorCount = generateDoorCount(uids.length);
    gameState.value.gold.total = doorCount.gold;
    gameState.value.fire.total = doorCount.fire;
    gameState.value.empty.total = doorCount.empty;
    setupRound();
  }

  function resetTemple() {
    gameState.value = buildInitGameState();
    gameOverInfo.value = null;
    gameOverResult.value = [];
  }

  function randomPickFirstKeyHolder() {
    const uids = playersInfo.value.uids;
    const uid = uids[Math.floor(Math.random() * uids.length)];
    gameState.value.keyholderUid = uid;
    updatePublicInfo();
  }

  function setupRound() {
    if (!room.value) {
      return;
    }
    gameState.value.round += 1;
    if (checkGameOver()) {
      return;
    }
    gameState.value.lastDoorOpen = undefined;
    gameState.value.status = 'playing';
    gameState.value.openedDoorCount = 0;
    const uids = playersInfo.value.uids;
    const doorCount = {
      gold: gameState.value.gold.total - gameState.value.gold.found,
      fire: gameState.value.fire.total - gameState.value.fire.found,
      empty: gameState.value.empty.total - gameState.value.empty.found
    };
    const pdMap = dispatchPlayerDoors(gameState.value.round, uids, doorCount);
    gameState.value.playerOpenDoorMap = pdMap.open;
    gameState.value.playerHoldingDoorMap = pdMap.holding;
    const [sendDoorDispatch] = room.value.makeAction('doorDispatch');
    for (const uid of uids) {
      const holdingDoors = pdMap.holding[uid];
      sendDoorDispatch(holdingDoors, playersInfo.value.peerIdMap[uid]);
    }
    updatePublicInfo();
  }

  function checkGameOver(): boolean {
    if (!room.value) {
      return false;
    }
    let goInfo: GameOverInfo | undefined;
    if (gameState.value.fire.found === gameState.value.fire.total) {
      goInfo = { winner: 'guardian', reason: 'fire' };
    }
    if (
      gameState.value.round <= 4 &&
      gameState.value.gold.found === gameState.value.gold.total
    ) {
      goInfo = { winner: 'adventurer', reason: 'gold' };
    }
    if (
      gameState.value.round === 4 &&
      gameState.value.gold.found < gameState.value.gold.total &&
      openedDoorCountInRound(playersInfo.value.uids.length, gameState.value) ===
        playersInfo.value.uids.length
    ) {
      goInfo = { winner: 'guardian', reason: 'timeout' };
    }
    if (goInfo) {
      gameState.value.status = 'gameover';
      gameOverInfo.value = goInfo;
      gameOverResult.value = [];
      playersInfo.value.uids.forEach((uid) => {
        gameOverResult.value.push({
          name: playersInfo.value.nameMap[uid],
          role: gameState.value.playerRoleMap[uid]
        });
      });
      const [sendGameOver] = room.value.makeAction('gameOver');
      sendGameOver(goInfo);
      updatePublicInfo();
      return true;
    } else {
      return false;
    }
  }

  function checkRoundEnd() {
    const numPlayers = playersInfo.value.uids.length;
    if (openedDoorCountInRound(numPlayers, gameState.value) === numPlayers) {
      gameState.value.status = 'next-round';
    }
  }

  async function updatePublicInfo() {
    if (!room.value) {
      return;
    }
    const publicInfo = JSON.parse(JSON.stringify(gameState.value));
    publicInfo['playerRoleMap'] = {};
    publicInfo['playerHoldingDoorMap'] = {};
    const [sendPublicUpdate] = room.value.makeAction('publicUpdate');
    sendPublicUpdate(publicInfo);
  }

  function myPeerId(): PeerId {
    return selfId;
  }

  function status(): PeerStatus {
    if (!room.value) {
      return 'none';
    }
    return isConnected.value ? 'connected' : 'ready';
  }

  return {
    code,
    error,
    gameState,
    playersInfo,
    gameOverInfo,
    gameOverResult,
    openArena,
    closeArena,
    myPeerId,
    status,
    setupGame,
    setupRound,
    updatePublicInfo,
    randomPickFirstKeyHolder
  };
});
