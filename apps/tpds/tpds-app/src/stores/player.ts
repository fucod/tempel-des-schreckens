import { ref } from 'vue';
import { defineStore } from 'pinia';
import { Room, joinRoom, selfId } from 'trystero';
import appStatic from '../static/app.json';
import {
  DoorChoice,
  DoorKind,
  GameOverInfo,
  GamePublicInfo,
  PlayerRole,
  PlayersInfo,
  buildInitGameState
} from '../services/game';
import { Uid, genUid } from '@base-workspace/bw-model';

type PlayerStatus = 'none' | 'registered' | 'keyholder';

type PeerId = string;

type PeerDoneMsg = {
  action: string;
  tUid: string;
};

type PeerError = {
  action: string;
  code: string;
};

export const usePlayerStore = defineStore('tpds-player-store', () => {
  const code = ref<string | null>(null);
  const room = ref<Room | null>(null);
  const playerUid = ref<Uid | null>(null);
  const playerName = ref<string>('');
  const playerStatus = ref<PlayerStatus>('none');
  const error = ref<PeerError | null>(null);
  const doneMsg = ref<PeerDoneMsg | null>(null);
  const templePeerId = ref<string | null>(null);
  const playerRole = ref<PlayerRole | null>(null);
  const publicInfo = ref<GamePublicInfo>(buildInitGameState());
  const gameOverInfo = ref<GameOverInfo | null>(null);
  const holdingDoors = ref<DoorKind[]>([]);
  const playersInfo = ref<PlayersInfo>({
    uids: [],
    nameMap: {},
    peerIdMap: {}
  });

  function assignPlayerName(name: string) {
    playerName.value = name;
  }

  function connect(connectCode: string): string {
    if (!playerName.value) {
      return 'player_name_required';
    }
    room.value = joinRoom(
      { appId: `${appStatic.appId}_${appStatic.version}` },
      connectCode
    );
    const [, getArenaError] = room.value.makeAction('arenaError');
    const [, getDone] = room.value.makeAction('done');
    const [, getArenaClose] = room.value.makeAction('arenaClose');
    const [, getRoleDispatch] = room.value.makeAction('roleDispatch');
    const [, getDoorDispatch] = room.value.makeAction('doorDispatch');
    const [, getPlayersInfo] = room.value.makeAction('playersInfo');
    const [, getPublicUpdate] = room.value.makeAction('publicUpdate');
    const [, getGameOver] = room.value.makeAction('gameOver');
    const [, getGameRestart] = room.value.makeAction('gameRestart');
    const [sendRegister] = room.value.makeAction('register');
    getArenaError(async (payload) => {
      error.value = payload as PeerError;
    });
    getDone(async (payload, peerId) => {
      doneMsg.value = payload as PeerDoneMsg;
      if (doneMsg.value.action === 'connect') {
        templePeerId.value = peerId;
        playerUid.value = genUid();
        playerStatus.value = 'registered';
        sendRegister({ name: playerName.value, uid: playerUid.value }, peerId);
      }
      doneMsg.value = null;
    });
    getArenaClose(async () => {
      publicInfo.value = buildInitGameState();
    });
    getRoleDispatch((role) => {
      playerRole.value = role as PlayerRole;
    });
    getDoorDispatch((doors) => {
      holdingDoors.value = doors as DoorKind[];
    });
    getPlayersInfo((info) => {
      playersInfo.value = info as PlayersInfo;
    });
    getPublicUpdate((info) => {
      publicInfo.value = info as GamePublicInfo;
    });
    getGameOver((info) => {
      gameOverInfo.value = info as GameOverInfo;
    });
    getGameRestart(() => {
      playerRole.value = null;
      publicInfo.value = buildInitGameState();
      gameOverInfo.value = null;
    });
    return 'ok';
  }

  function disconnect() {
    if (!room.value) {
      return;
    }
    const [sendDisconnect] = room.value.makeAction('disconnect');
    sendDisconnect(playerUid.value, templePeerId.value);
    room.value.leave();
    room.value = null;
    playerStatus.value = 'none';
  }

  function openDoor(dc: DoorChoice) {
    if (!room.value) {
      return;
    }
    const [sendDoorOpen] = room.value.makeAction('doorOpen');
    sendDoorOpen(dc, templePeerId.value);
  }

  function myPeerId(): PeerId {
    return selfId;
  }

  function isKeyHolder(): boolean {
    return isMe(publicInfo.value.keyholderUid);
  }

  function isMe(uid: Uid | undefined): boolean {
    return uid === playerUid.value;
  }

  return {
    code,
    error,
    playerStatus,
    playerRole,
    playersInfo,
    publicInfo,
    holdingDoors,
    gameOverInfo,
    assignPlayerName,
    connect,
    disconnect,
    openDoor,
    myPeerId,
    isKeyHolder,
    isMe
  };
});
