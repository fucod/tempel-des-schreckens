import { createApp } from 'vue';
import { createWebHashHistory, createRouter } from 'vue-router';
import { createPinia } from 'pinia';
import { Startup } from '@base-workspace/bw-core';
import { generateI18n } from '@base-workspace/bw-locale';
import { LOCALE_MAP } from './locales/index';
import { routes } from './services/routes';
import buildMetadata from './build/metadata.json';
import App from './components/app/App.vue';

import './assets/css/index.css';

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

router.beforeEach((to) => {
  if (to.name !== 'entry' && !Startup.checkReady()) {
    return { name: 'entry' };
  }
});

const i18n = generateI18n(LOCALE_MAP, buildMetadata.locale);

const pinia = createPinia();

const app = createApp(App);
app.use(i18n);
app.use(router);
app.use(pinia);
app.mount('#root');
