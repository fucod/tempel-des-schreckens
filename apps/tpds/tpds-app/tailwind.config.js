const { createGlobPatternsForDependencies } = require('@nx/vue/tailwind');
const { join } = require('path');

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    join(__dirname, 'index.html'),
    join(__dirname, 'src/**/*!(*.stories|*.spec).{vue,ts,tsx,js,jsx}'),
    ...createGlobPatternsForDependencies(__dirname)
  ],
  theme: {
    extend: {}
  },
  plugins: [],
  ...require('../../../modules/bw-core/tailwind.config.js')
};

module.exports.daisyui.themes.push({
  tpds: {
    'color-scheme': 'light',
    primary: '#78350f',
    secondary: '#a16207',
    accent: '#d97706',
    neutral: '#181a2a',
    info: '#2794c1',
    success: '#32956f',
    warning: '#d1950b',
    error: '#e32d2d',
    'neutral-content': '#edf2f7',
    'base-100': '#fef3c7',
    'base-200': '#f9d25f',
    'base-300': '#e2bf56',
    'base-400': '#cbac4d',
    'base-content': '#351706',
    '--rounded-box': '0.25rem',
    '--rounded-btn': '.2rem',
    '--rounded-badge': '.125rem',
    '--animation-btn': '0.2s',
    '--animation-input': '0',
    '--btn-focus-scale': '0.95',
    '--btn-text-case': 'none'
  }
});
