# Base Workspace

A base workspace for developing web applications based on vue + tailwind + typescript.

# Structure

The project is a monorepo managed by Nx.

## Applications

- Applications are list in `apps`. An applicaiton should be located in a folder name `xxx` which contains `xxx-app` (The main application) and `xxx-app-e2e` (E2E tests). Please reference `apps/ex` for example.
- The `<app-name>` is defined in `apps/xxx/xxx-app/project.json`.

## Modules

- Modules are listed in `modules`. A modules should be located in a folder named `modules/bw-xxx`.
- The `tsconfig.base.json` in root folder defines all modules in its paths.
- The `<module-name>` is defined in `modules/bw-xxx/project.json`.

# Development

## Prerequisites

- Nix

## Prepare development environment

1. Run `nix-shell` to setup node environment.
2. In nix-shell, run `npm install` to install npm package dependency.

## Commands for Apps

### Run the application dev server

`npx nx serve <app-name>`

### Build application

`npx nx build <app-name>`

### Run test

`npx nx test <app-name>`

## Commands for Modules

### Generate doc

`npx nx gendoc <modules-name>`

### Run test

`npx nx test <modules-name>`

## Create a new application

1. Run `nx g @nx/vue:application xxx-app --directory=apps/xxx/xxx-app` to create a new App.
2. Run `nx g @nx/vue:setup-tailwind --project=xxx-app` to setup tailwind css. To apply common tailwind settings, append the common tailwind config in `apps/xxx/xxx-app/tailwind.config.js` as below.

apps/xxx/xxx-app/tailwind.config.js

```js
const { createGlobPatternsForDependencies } = require('@nx/vue/tailwind');
const { join } = require('path');

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [join(__dirname, 'index.html'), join(__dirname, 'src/**/*!(*.stories|*.spec).{vue,ts,tsx,js,jsx}'), ...createGlobPatternsForDependencies(__dirname)],
  theme: {
    extend: {},
  },
  plugins: [],
  // Here
  ...require('../../../modules/bw-core/tailwind.config.js'),
};
```

3. To apply common assets (such as background images used by theme), add `publicDir` config in `apps/xxx/xxx-app/vite.config.ts` as below.

apps/xxx/xxx-app/vite.config.ts

```ts
/// <reference types='vitest' />
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { nxViteTsPaths } from '@nx/vite/plugins/nx-tsconfig-paths.plugin';

export default defineConfig({
  root: __dirname,
  cacheDir: '../../../node_modules/.vite/apps/ex/ex-app',
  // Here
  publicDir: '../../../modules/bw-core/public',

  server: {
    port: 4200,
    host: 'localhost',
  },

  preview: {
    port: 4300,
    host: 'localhost',
  },

  plugins: [vue(), nxViteTsPaths()],

  // Uncomment this if you are using workers.
  // worker: {
  //  plugins: [ nxViteTsPaths() ],
  // },

  build: {
    outDir: '../../../dist/apps/xxx/xxx-app',
    reportCompressedSize: true,
    commonjsOptions: {
      transformMixedEsModules: true,
    },
  },

  test: {
    globals: true,
    cache: {
      dir: '../../../node_modules/.vitest/apps/xxx/xxx-app',
    },
    environment: 'jsdom',
    include: ['src/**/*.{test,spec}.{js,mjs,cjs,ts,mts,cts,jsx,tsx}'],

    reporters: ['default'],
    coverage: {
      reportsDirectory: '../../../coverage/apps/xxx/xxx-app',
      provider: 'v8',
    },
  },
});
```

4. To apply the default css styles from bw-core and bw-style, Create `index.css` in `apps/xxx/xxx-app/src/assets/css` as below (Please reference `apps/ex/ex-app/src/assets/css` as well) and modify `apps/xxx/xxx-app/src/main.ts` to import css index.

```
xxx/xxx-app/src/assets/css
├── app.css
└── index.css
```

xxx/xxx-app/src/assets/css/index.css

```css
@import '../../../../../../modules/bw-core/src/assets/css/core.css';
@import '../../../../../../modules/bw-theme/src/assets/css/theme.css';
@import './app.css';

@tailwind base;
@tailwind components;
@tailwind utilities;
```

xxx/xxx-app/src/assets/css/app.css

```css
/* app-scoped styles*/
```

xxx/xxx-app/src/main.ts

```ts
import { createApp } from 'vue';
import { createWebHistory, createRouter } from 'vue-router';
import { createPinia } from 'pinia';
import { Startup } from '@base-workspace/bw-core';
import { generateI18n } from '@base-workspace/bw-locale';
import { routes } from './services/routes';
import { LOCALE_MAP } from './locales/index';
import App from './components/app/App.vue';

// Here
import './assets/css/index.css';

const app = createApp(App);
const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to) => {
  if (to.name !== 'entry' && !Startup.checkReady()) {
    return { name: 'entry' };
  }
});
const i18n = generateI18n(LOCALE_MAP, 'en-US');
const pinia = createPinia();

app.use(i18n);
app.use(router);
app.use(pinia);
app.mount('#root');
```

5. Modify files in `src/`. Please reference `apps/ex/ex-app/src`.

## Create a new module

1. Use Nx commands to generate a new module.

- If the module contains vue components, Use

`npx nx g @nx/vue:lib bw-xxx --directory=modules/bw-xxx`

- If the module a pure js library, Use

`npx nx g @nx/js:lib bw-xxx --directory=modules/bw-xxx`

2. To support doc generation, create a ts config in `modules/bw-xxx/tsconfig.doc.json` and add a `gendoc` target in `modules/bw-xxx/project.json`

modules/bw-xxx/tsconfig.doc.json

```json
{
  "compilerOptions": {
    "lib": ["es2020", "dom"]
  },
  "include": ["src/**/*.ts"],
  "typedocOptions": {
    "name": "bw-xxx",
    "entryPoints": ["./src/index.ts"],
    "out": "docs"
  }
}
```

modules/bw-xxx/project.json

```json
{
  "name": "bw-xxx",
  "$schema": "../../node_modules/nx/schemas/project-schema.json",
  "sourceRoot": "modules/bw-xxx/src",
  "projectType": "library",
  "tags": [],
  "// targets": "to see all targets run: nx show project bw-xxx--web",
  "targets": {
    "gendoc": {
      "command": "typedoc --tsconfig {projectRoot}/tsconfig.doc.json"
    }
  }
}
```
