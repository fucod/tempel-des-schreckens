import enUS from './enUS.js';
import jaJP from './jaJP.js';
import zhCN from './zhCN.js';
import zhTW from './zhTW.js';

export const LOCALE_MAP = {
  'en-US': enUS,
  'ja-JP': jaJP,
  'zh-CN': zhCN,
  'zh-TW': zhTW
};
