import timeZoneData from './data/geo.json';

type TimeZoneData = {
  continent: string;
  city: string;
  longitude: number;
  latitude: number;
  labelAdjustment?: {
    x?: number;
    y?: number;
    anchor?: string;
  };
};

export type TimeZoneValue = keyof typeof timeZoneData;

export const TIME_ZONES = Object.keys(timeZoneData) as TimeZoneValue[];

export const DEFAULT_TIMEZONE: TimeZoneValue = 'Asia/Tokyo';

export function detectLocalTz(): TimeZoneValue {
  return (
    (Intl.DateTimeFormat().resolvedOptions().timeZone as TimeZoneValue) ||
    DEFAULT_TIMEZONE
  );
}

export function fetchTzData(timeZone: TimeZoneValue): TimeZoneData {
  return timeZoneData[timeZone];
}

export function fetchTzOffset(timeZone: TimeZoneValue): string {
  const partMap = buildDateFormatPartMap(new Date(), timeZone as string);
  return partMap['timeZoneName'];
}

export function formatDateWithTz(timeZone: string, date?: Date): string {
  const givenDt = date || new Date();
  const partMap = buildDateFormatPartMap(givenDt, timeZone);
  return `${partMap['year']}.${partMap['month']}.${partMap['day']} ${partMap['hour']}:${partMap['minute']}:${partMap['second']}`;
}

function buildDateFormatPartMap(
  date: Date,
  timeZone: string
): Record<string, string> {
  const formatter = new Intl.DateTimeFormat('en-US', {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    hour12: false,
    timeZone,
    timeZoneName: 'longOffset'
  });
  const parts = formatter.formatToParts(date);
  const partMap: Record<string, string> = {};
  for (let i = 0; i < parts.length; i++) {
    const part = parts[i];
    if (part.type !== 'literal') {
      partMap[part.type] = part.value;
    }
  }
  return partMap;
}
