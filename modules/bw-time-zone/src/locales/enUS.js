import data from './data/en-US.json' assert { type: 'json' };

export default {
  data
};
