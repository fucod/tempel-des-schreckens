import feature from './feature/en-US.json' assert { type: 'json' };
import components from './components/en-US.json' assert { type: 'json' };

export default {
  feature,
  components
};
