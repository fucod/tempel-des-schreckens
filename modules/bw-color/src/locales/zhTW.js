import feature from './feature/zh-TW.json' assert { type: 'json' };
import components from './components/zh-TW.json' assert { type: 'json' };

export default {
  feature,
  components
};
