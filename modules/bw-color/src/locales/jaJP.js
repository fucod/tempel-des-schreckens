import feature from './feature/ja-JP.json' assert { type: 'json' };
import components from './components/ja-JP.json' assert { type: 'json' };

export default {
  feature,
  components
};
