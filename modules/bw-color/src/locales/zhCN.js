import feature from './feature/zh-CN.json' assert { type: 'json' };
import components from './components/zh-CN.json' assert { type: 'json' };

export default {
  feature,
  components
};
