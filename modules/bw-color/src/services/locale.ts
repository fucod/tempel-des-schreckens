import { useI18n } from 'vue-i18n';
import { LocaleActor } from '@base-workspace/bw-locale';
import { LOCALE_MAP } from '../locales/index';
import { type ColorMapKey, type ColorValue, COLOR_MAP } from '../def';

export function fetchColorName(color: ColorValue): string {
  const i18n = useI18n({ useScope: 'local', messages: LOCALE_MAP });
  const la = new LocaleActor('components.CameraCaptureModal', i18n.t);
  if (color === undefined || color === '') {
    return la.t('feature.color.name.none') as string;
  }
  const cData = COLOR_MAP[color.slice(0, 7) as ColorMapKey];
  if (cData === undefined) {
    return la.t('feature.color.name.none') as string;
  }
  let nameKey = `feature.color.name.${cData.name}`;
  if (cData.name.startsWith('gray')) {
    nameKey = 'feature.color.name.gray';
  }
  let variant = '';
  if (cData.variant === 'light' || cData.variant === 'deep') {
    const variantKey = `feature.color.variant.${cData.variant}`;
    variant = la.t(variantKey) as string;
    return la.t('feature.color.format', {
      name: la.t(nameKey) as string,
      variant
    }) as string;
  }
  return la.t(nameKey) as string;
}
