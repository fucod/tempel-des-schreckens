import { z, ZodSchema } from 'zod';
import colorData from './data.json';

export type ColorMapKey = keyof typeof colorData;
export type ColorValue = string | undefined;

export type ColorSelectorOption = {
  colors: ColorValue[];
  reverse: boolean;
  inRowSize: number;
  showNoneBtn: boolean;
};

export const COLOR_PICKER_PROP = {
  current: {
    type: String,
    required: false
  },
  enableAlpha: {
    type: Boolean,
    default: false
  },
  reverseLayout: {
    type: Boolean,
    default: false
  },
  storeKey: {
    type: String,
    default: 'color-picker-store'
  }
};

export const COLOR_MAP = colorData;

export const COLORS = Object.keys(colorData) as ColorValue[];
export const COLOR_ROW_SIZE = 10;
export const DEEP_COLORS: ColorValue[] = fetchColorSet('deep');
export const LIGHT_COLORS: ColorValue[] = fetchColorSet('light');
export const NORMAL_COLORS: ColorValue[] = fetchColorSet('none');
export const GRAY_COLORS: ColorValue[] = fetchColorSet('gray');
export const NOT_BW_COLORS = COLORS.filter(
  (color) => !['#ffffff', '#000000'].includes(color as string)
);

function fetchColorSet(variant: string): ColorValue[] {
  return Object.keys(colorData).filter((color) => {
    const cData = colorData[color as ColorMapKey];
    return cData.variant === variant;
  }) as ColorValue[];
}

export const DEFAULT_COLOR_SELECTOR_OPTION: ColorSelectorOption = {
  colors: COLORS,
  reverse: false,
  inRowSize: 5,
  showNoneBtn: true
};

export const ColorSchema: ZodSchema = z
  .string()
  .regex(/^#([A-Fa-f0-9]{6})$/)
  .optional();
