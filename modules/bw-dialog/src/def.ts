import type { PropType } from 'vue';

export type DialogOption = {
  klass?: string;
  position?: string;
  enableCloseBtn?: boolean;
  enableOutsideClose?: boolean;
};

export type DialogOptionParams = Partial<DialogOption>;

export const DEFAULT_DIALOG_OPTION: DialogOption = {
  klass: '',
  enableCloseBtn: true,
  enableOutsideClose: false
};

export const DIALOG_OPTION_PROP = {
  dialogOption: {
    type: Object as PropType<DialogOptionParams>,
    default: DEFAULT_DIALOG_OPTION
  }
};
