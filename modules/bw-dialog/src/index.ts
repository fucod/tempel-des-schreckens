export type { DialogOption } from './def';
export { DIALOG_OPTION_PROP } from './def';
export { default as BaseDialog } from './components/BaseDialog.vue';
export { default as CameraCaptureDialog } from './components/CameraCaptureDialog.vue';
export { default as SelectorDialog } from './components/SelectorDialog.vue';
export { default as AutoCompletionDialog } from './components/AutoCompletionDialog.vue';
export { default as LocaleSelectorDialog } from './components/LocaleSelectorDialog.vue';
