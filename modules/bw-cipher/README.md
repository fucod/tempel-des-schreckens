# bw-cipher

Provide ciphers for common encryptions.

# Implementation details

{@link RsaCipher} and {@link AesCipher} are provided by jose, and {@link BcryptCipher} is by bcryptjs.

# Development

## Run tests

- `nx test bw-cipher`

## Generate doc

- `nx gendoc bw-cipher`
