import { faker } from '@faker-js/faker';
import { AesCipher } from './aes';

describe('AesCipher', () => {
  it('encrypts and decrypts data by given key', async () => {
    const key = (await AesCipher.genKey()) as string;
    const data = faker.lorem.paragraph();
    const encryptedData = await AesCipher.encrypt(key, data);
    const decryptedData = await AesCipher.decrypt(key, encryptedData);
    expect(encryptedData).not.toEqual(data);
    expect(decryptedData).toEqual(data);
  });
});
