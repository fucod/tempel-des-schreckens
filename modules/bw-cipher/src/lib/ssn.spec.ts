import { faker } from '@faker-js/faker';
import { SsnCipher } from './ssn';

describe('SsnCipher', () => {
  it('encrypts and decrypts data by given key', () => {
    const key = SsnCipher.genKey();
    const data = faker.lorem.paragraph();
    const encryptedData = SsnCipher.encrypt(key, data);
    const decryptedData = SsnCipher.decrypt(key, encryptedData);
    expect(encryptedData).not.toEqual(data);
    expect(decryptedData).toEqual(data);
  });
});
