import * as jose from 'jose';

const JWE_ALG = 'A256KW';
const JWE_ENC = 'A256GCM';

/** Generate AES key. */
async function genKey(): Promise<string | undefined> {
  const key = await jose.generateSecret(JWE_ALG, {
    extractable: true
  });
  return (await jose.exportJWK(key)).k;
}

/** Encrypt data with the given key. */
async function encrypt(key: string, data: string): Promise<string> {
  const jwk = { k: key, kty: 'oct' };
  const importedKey = await jose.importJWK(jwk, JWE_ALG);
  return await new jose.CompactEncrypt(new TextEncoder().encode(data))
    .setProtectedHeader({ alg: JWE_ALG, enc: JWE_ENC })
    .encrypt(importedKey);
}

/** Decrypt encrypted data with the given key. */
async function decrypt(key: string, encryptedData: string): Promise<string> {
  const jwk = { k: key, kty: 'oct' };
  const importedKey = await jose.importJWK(jwk, JWE_ALG);
  const { plaintext } = await jose.compactDecrypt(encryptedData, importedKey);
  return new TextDecoder().decode(plaintext);
}

/**
 * Encrypt data with AES.
 *
 * @example
 * ```ts
 * const data = "aaa";
 * const key = await AesCipher.genKey();
 * const encryptedData = await AesCipher.encrypt(key, data);
 * const decryptedData = await AesCipher.decrypt(key, encryptedData);
 * ```
 */
export const AesCipher = {
  genKey,
  encrypt,
  decrypt
} as const;
