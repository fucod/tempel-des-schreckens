import * as bcrypt from 'bcryptjs';

/** Hash data with Bcrypt hashing function */
function hash(data: string): string {
  const salt = bcrypt.genSaltSync();
  return bcrypt.hashSync(data, salt);
}

/** Compare hashed data with original one */
function compare(data: string, hashedData: string): boolean {
  return bcrypt.compareSync(data, hashedData);
}

/**
 * Hash data with Bcrypt.
 *
 * @example
 * ```ts
 * const data = "aaa";
 * const hashedData = BcryptCipher.hash(data);
 * const isMatched = BcryptCipher.compare(data, hashedData);
 * ```
 */
export const BcryptCipher = {
  hash,
  compare
} as const;
