import { faker } from '@faker-js/faker';
import { RsaCipher } from './rsa';

describe('RsaCipher', () => {
  it('encrypts and decrypts data by given key', async () => {
    const keys = await RsaCipher.genKey();
    const data = faker.lorem.paragraph();
    const encryptedData = await RsaCipher.encrypt(keys['public'], data);
    const decryptedData = await RsaCipher.decrypt(
      keys['private'],
      encryptedData
    );
    expect(encryptedData).not.toEqual(data);
    expect(decryptedData).toEqual(data);
  });
});
