/** Generate Substitution key. */
function genKey(
  charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
): string {
  return [...new Set(charSet)]
    .sort(function () {
      return 0.5 - Math.random();
    })
    .join('');
}

/** Encrypt data with the given key. */
function encrypt(key: string, data: string): string {
  const keyChars = key.split('');
  const sortChars = key.split('').sort();
  return data
    .split('')
    .map((char) => {
      const foundIndex = sortChars.indexOf(char);
      if (foundIndex >= 0) {
        return keyChars[foundIndex];
      } else {
        return char;
      }
    })
    .join('');
}

/** Decrypt encrypted data with the given key. */
function decrypt(key: string, encryptedData: string): string {
  const keyChars = key.split('');
  const sortChars = key.split('').sort();
  return encryptedData
    .split('')
    .map((char) => {
      const foundIndex = keyChars.indexOf(char);
      if (foundIndex >= 0) {
        return sortChars[foundIndex];
      } else {
        return char;
      }
    })
    .join('');
}

/**
 * Encrypt data with Substitution.
 *
 * @example
 * ```ts
 * const data = "aaa";
 * const key = SsnCipher.genKey();
 * const encryptedData = SsnCipher.encrypt(key, data);
 * const decryptedData = SsnCipher.decrypt(key, encryptedData);
 * ```
 */
export const SsnCipher = {
  genKey,
  encrypt,
  decrypt
} as const;
