import { faker } from '@faker-js/faker';
import { BcryptCipher } from './bcrypt';

describe('BcryptCipher', () => {
  it('hashes and compares data by using bcrypt algorithm', async () => {
    const data = faker.lorem.paragraph();
    const hashedData = BcryptCipher.hash(data);
    expect(BcryptCipher.compare(data, hashedData)).toBeTruthy();
    const wrongData = faker.lorem.paragraph();
    expect(BcryptCipher.compare(wrongData, hashedData)).toBeFalsy();
  });
});
