import * as jose from 'jose';

const JWE_ALG = 'RSA-OAEP-256';
const JWE_ENC = 'A256GCM';

/** Generate key for RSA. */
async function genKey(): Promise<{
  private: string;
  public: string;
}> {
  const { publicKey, privateKey } = await jose.generateKeyPair(JWE_ALG, {
    extractable: true
  });
  return {
    public: await jose.exportSPKI(publicKey),
    private: await jose.exportPKCS8(privateKey)
  };
}

/** Encrypt data with the public key. */
async function encrypt(publicKey: string, data: string): Promise<string> {
  const key = await jose.importSPKI(publicKey, JWE_ALG);
  return await new jose.CompactEncrypt(new TextEncoder().encode(data))
    .setProtectedHeader({ alg: JWE_ALG, enc: JWE_ENC })
    .encrypt(key);
}

/** Decrypt encrypted data with the private key. */
async function decrypt(
  privateKey: string,
  encryptedData: string
): Promise<string> {
  const key = await jose.importPKCS8(privateKey, JWE_ALG);
  const { plaintext } = await jose.compactDecrypt(encryptedData, key);
  return new TextDecoder().decode(plaintext);
}

/**
 * Encrypt data with RSA.
 *
 * @example
 * ```ts
 * const data = "aaa";
 * const { private, public } = await RsaCipher.genKey();
 * const encryptedData = await RsaCipher.encrypt(public, data);
 * const decryptedData = await RsaCipher.decrypt(private, encryptedData);
 * ```
 */
export const RsaCipher = {
  genKey,
  encrypt,
  decrypt
} as const;
