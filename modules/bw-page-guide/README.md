# bw-page-guide

A module to provide page-related types and functions.

# What & Why

- Use a `PageGuideData[]` array (called `PageDef`) to define basic information such as title, icon, routes for each pages.
- `PageDef` is used to generate page headers, links, menu entries for pages.

# How

## An example of `PageDef`

`<app-folder>/src/services/page-def.ts`

```typescript
import * as PageGuide from '@base-workspace/bw-page-guide';
import mdiViewDashboard from '@iconify-icons/mdi/view-dashboard';
import mdiPalette from '@iconify-icons/mdi/palette';
import mdiStar from '@iconify-icons/mdi/star';
import mdiCog from '@iconify-icons/mdi/cog';
import mdiFormTextbox from '@iconify-icons/mdi/form-textbox';
import mdiViewDashboardEdit from '@iconify-icons/mdi/view-dashboard-edit';
import mdiHome from '@iconify-icons/mdi/home';

export const PAGES: PageGuide.PageGuideData[] = [
  {
    code: 'demo',
    route: { name: 'demo' },
    nameLk: 'pages.DemoPage.title',
    iconInfo: {
      icon: mdiViewDashboard,
    },
  },
  {
    code: 'style',
    route: { name: 'style' },
    nameLk: 'pages.StylePage.title',
    iconInfo: {
      icon: mdiPalette,
    },
  },
  {
    code: 'about',
    route: { name: 'about' },
    nameLk: 'pages.AboutPage.title',
    iconInfo: {
      icon: mdiStar,
    },
  },
  {
    code: 'settings',
    route: { name: 'settings' },
    nameLk: 'pages.settings.title',
    iconInfo: {
      icon: mdiCog,
    },
  },
  {
    code: 'settings-format',
    parent: 'settings',
    route: { name: 'settings', query: { tab: 'settings-format' } },
    nameLk: 'pages.settings.tab.format',
    iconInfo: {
      icon: mdiFormTextbox,
    },
  },
  {
    code: 'settings-display',
    parent: 'settings',
    route: { name: 'settings', query: { tab: 'settings-display' } },
    nameLk: 'pages.settings.tab.display',
    iconInfo: {
      icon: mdiViewDashboardEdit,
    },
  },
  {
    code: 'entry',
    route: { name: 'entry' },
    nameLk: 'app.service.home',
    iconInfo: {
      icon: mdiHome,
    },
  },
];
```

# Development

## Run tests

- `nx test bw-page-guide`

## Generate doc

- `nx gendoc bw-page-guide`
