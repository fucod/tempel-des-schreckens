import type { IconInfo } from '@base-workspace/bw-icon';
import type { RouteLocationRaw } from 'vue-router';

export type PageGuideData = {
  /** The page code. It should be unique for each pages. */
  code: string;

  /** The page route used in page links or menu entries. */
  route: RouteLocationRaw;

  /** The name-link for page title. The i18n title should be defined in `<page-code>.title` in `<app-folder>/src/locales/pages/<locale>.json` */
  nameLk: string;

  /** The page icon. */
  iconInfo: IconInfo;

  /** The parent page code. For menu hierarchy display. */
  parent?: string;
};

/**
 * Build a map of page codes to {@link PageGuideData}
 */
export function buildPageMap(
  pageDef: PageGuideData[]
): Record<string, PageGuideData> {
  const pageMap: Record<string, PageGuideData> = {};
  for (const pageRaw of pageDef) {
    pageMap[pageRaw.code] = {
      ...pageRaw
    };
  }
  return pageMap;
}

/**
 * Build an array of {@link PageGuideData} based on provided page codes
 */
export function buildPageList(
  pageDef: PageGuideData[],
  pages: string[] = []
): PageGuideData[] {
  return pages.map((pageKey) => {
    return buildPageMap(pageDef)[pageKey];
  });
}

/**
 * Build an array of {@link PageGuideData} based on the given parent page code
 */
export function buildPageListByParent(
  pageDef: PageGuideData[],
  parent: string
): PageGuideData[] {
  const pages = pageDef
    .filter((pageRaw) => pageRaw.parent === parent)
    .map((pageRaw) => pageRaw.code);
  return buildPageList(pageDef, pages);
}

/**
 * Return the {@link PageGuideData} for a given page code
 */
export function fetchPageData(
  pageDef: PageGuideData[],
  code: string
): PageGuideData | null {
  for (const pageRaw of pageDef) {
    if (pageRaw.code === code) {
      return pageRaw;
    }
  }
  return null;
}
