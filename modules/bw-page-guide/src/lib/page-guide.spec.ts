import {
  buildPageMap,
  buildPageList,
  buildPageListByParent,
  fetchPageData,
  PageGuideData
} from './page-guide'; // Replace './yourModule' with the actual path of your module

describe('PageGuide', () => {
  describe('.buildPageMap', () => {
    it('creates a map of page codes to PageGuideData', () => {
      const pageDef: PageGuideData[] = [
        {
          code: 'home',
          route: '/home',
          nameLk: 'Home',
          iconInfo: {
            icon: { body: '' }
          },
          parent: 'main'
        },
        {
          code: 'about',
          route: '/about',
          nameLk: 'About',
          iconInfo: {
            icon: { body: '' }
          },
          parent: 'main'
        }
      ];
      const result = buildPageMap(pageDef);
      expect(result).toHaveProperty('home');
      expect(result).toHaveProperty('about');
      expect(result['home'].route).toBe('/home');
      expect(result['about'].route).toBe('/about');
    });
  });

  describe('.buildPageList', () => {
    it('returns an array of PageGuideData based on provided page keys', () => {
      const pageDef: PageGuideData[] = [
        {
          code: 'home',
          route: '/home',
          nameLk: 'Home',
          iconInfo: {
            icon: { body: '' }
          }
        },
        {
          code: 'contact',
          route: '/contact',
          nameLk: 'Contact',
          iconInfo: {
            icon: { body: '' }
          }
        }
      ];
      const pages = ['home'];
      const result = buildPageList(pageDef, pages);
      expect(result).toHaveLength(1);
      expect(result[0].code).toBe('home');
    });
  });

  describe('.buildPageListByParent', () => {
    it('filters PageGuideData by parent and returns an array', () => {
      const pageDef: PageGuideData[] = [
        {
          code: 'home',
          route: '/home',
          nameLk: 'Home',
          iconInfo: {
            icon: { body: '' }
          },
          parent: 'main'
        },
        {
          code: 'about',
          route: '/about',
          nameLk: 'About',
          iconInfo: {
            icon: { body: '' }
          },
          parent: 'secondary'
        }
      ];
      const parent = 'main';
      const result = buildPageListByParent(pageDef, parent);
      expect(result).toHaveLength(1);
      expect(result[0].parent).toBe('main');
    });
  });

  describe('.fetchPageData', () => {
    it('returns the PageGuideData for a given code', () => {
      const pageDef: PageGuideData[] = [
        {
          code: 'home',
          route: '/home',
          nameLk: 'Home',
          iconInfo: {
            icon: { body: '' }
          }
        },
        {
          code: 'about',
          route: '/about',
          nameLk: 'About',
          iconInfo: {
            icon: { body: '' }
          }
        }
      ];
      const code = 'about';
      const result = fetchPageData(pageDef, code);
      expect(result).toBeDefined();
      expect(result?.code).toBe('about');
    });

    it('returns null if the code does not exist', () => {
      const pageDef: PageGuideData[] = [
        {
          code: 'home',
          route: '/home',
          nameLk: 'Home',
          iconInfo: {
            icon: { body: '' }
          }
        }
      ];
      const code = 'nonexistent';
      const result = fetchPageData(pageDef, code);
      expect(result).toBeNull();
    });
  });
});
