import core from './core/en-US.json' assert { type: 'json' };

export default {
  locale: {
    name: 'English',
    title: 'Language'
  },
  core
};
