import core from './core/zh-TW.json' assert { type: 'json' };

export default {
  locale: {
    name: '繁體中文',
    title: '語言'
  },
  core
};
