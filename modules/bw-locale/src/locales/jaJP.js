import core from './core/ja-JP.json' assert { type: 'json' };

export default {
  locale: {
    name: '日本語',
    title: '言語'
  },
  core
};
