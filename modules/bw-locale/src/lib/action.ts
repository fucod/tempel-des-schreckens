import type { Composer, I18n } from 'vue-i18n';
import { createI18n } from 'vue-i18n';
import { CORE_LOCALE_MAP } from '../locales/index';

export const DEFAULT_LOCALE = 'en-US';

/**
 * Generate I18n instance by providing a locale map. The {@link CORE_LOCALE_MAP} would be included by default.
 *
 * @param localeMap - A locale map. It should be a map with locale keys. See {@link CORE_LOCALE_MAP} as an example.
 * @param appLocale - The default locale provided by App configuration. It is for building a App embedded a particular locale by default.
 *
 * @example
 * ```ts
 * import { createApp } from 'vue';
 * import { generateI18n } from '@base-workspace/feature-locale';
 * import { LOCALE_MAP }from './locales';
 * import App from './app/App.vue';
 *
 * const app = createApp(App);
 * const i18n = generateI18n(LOCALE_MAP, 'en');
 *
 *  app.use(i18n);
 *  app.mount('#root');
 * ```
 */
export function generateI18n(
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  localeMap: Record<string, any>,
  appLocale?: string
): I18n {
  const locales = Object.keys(CORE_LOCALE_MAP);
  for (const locale of locales) {
    if (localeMap[locale]) {
      localeMap[locale] = Object.assign(
        structuredClone(CORE_LOCALE_MAP[locale]),
        localeMap[locale]
      );
    }
  }
  return createI18n({
    legacy: false,
    locale: appLocale || DEFAULT_LOCALE,
    fallbackLocale: appLocale || DEFAULT_LOCALE,
    missingWarn: false,
    fallbackWarn: false,
    messages: localeMap
  });
}

/**
 * Detect the locale by `window.navigator.language`, If the detected locale is not supported, it would try to use the fallback or default locale.
 */
export function detectLocalLocale(i18n: Composer): string {
  const preferredLangs = window.navigator.languages;
  const localLocale = preferredLangs.filter((value) => {
    return i18n.availableLocales.includes(value);
  })[0];
  return localLocale || (i18n.fallbackLocale.value as string) || DEFAULT_LOCALE;
}
