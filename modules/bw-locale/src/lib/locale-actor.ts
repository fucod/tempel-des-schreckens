import { useI18n } from 'vue-i18n';
import type { ComposerTranslation } from 'vue-i18n';

const NEXT_SCOPE_KEY = 'NEXT_SCOPE_KEY';

/**
 * A `t` wrapper to supported the prefixes namespace feature.
 *
 * @example
 * ```ts
 * import { LocaleActor } from '@base-workspace/feature-locale';
 *
 * const la = LocaleActor("a");
 *
 * la.t("a.b.c"); // Fetch the locale string by key "a.b.c"
 * la.t(".b.c"); // Fetch the locale string by key "a.b.c"
 * ```
 */
export class LocaleActor {
  prefixes: string[];
  private _t: ComposerTranslation;

  constructor(prefixes: string | string[] = '', givenT?: ComposerTranslation) {
    if (typeof prefixes === 'string' || prefixes instanceof String) {
      this.prefixes = [prefixes as string];
    } else {
      this.prefixes = prefixes;
    }
    if (givenT) {
      this._t = givenT;
    } else {
      const { t } = useI18n({ useScope: 'global' });
      this._t = t;
    }
  }

  t(
    key: string,
    options: Record<string, number | string> = {}
  ): string | string[] {
    const heading = key.match(/(^\.+)/g);
    let scopeKey = key;
    if (heading === null) {
      return this._t(scopeKey, options);
    }
    for (let i = 0; i < this.prefixes.length; i++) {
      const prefix = this.prefixes[i];
      if (heading[0].length === 1) {
        scopeKey = `${prefix}${key}`;
      } else {
        const prefixArr = prefix
          .split('.')
          .slice(0, -1 * (heading[0].length - 1));
        scopeKey = `${prefixArr.join('.')}.${key.replace(/\.+/g, '')}`;
      }
      const translatedMsg = this._t(scopeKey, options, NEXT_SCOPE_KEY);
      if (translatedMsg !== NEXT_SCOPE_KEY) {
        return translatedMsg;
      }
    }
    return this._t(scopeKey, options);
  }
}
