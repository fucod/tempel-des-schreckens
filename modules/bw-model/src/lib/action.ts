/* eslint-disable @typescript-eslint/no-explicit-any */
import { toRaw } from 'vue';
import { ZodSchema } from 'zod';
import { CheckFieldResult, SortOpt } from './type';

export function handleModelError(
  error: Error,
  callback: (code: string, name: string) => void
) {
  switch (error.name) {
    case 'ModifyError':
      callback('modify_error', error.name);
      break;
    case 'ConstraintError':
      callback('constraint_error', error.name);
      break;
    default:
      callback('unknown_error', error.name);
  }
}

export function checkRequiredField<T>(
  requiredFields: (keyof T)[],
  doc: T
): boolean {
  for (const field of requiredFields) {
    const fieldValue = doc[field as keyof T];
    if (isUndefinedOrNull(fieldValue)) {
      return false;
    }
  }
  return true;
}

export function validateParams<T>(
  params: T,
  validationMap: {
    [key in keyof T]?: ZodSchema;
  }
): CheckFieldResult {
  for (const field of Object.keys(validationMap)) {
    const validator = validationMap[field as keyof T] as ZodSchema;
    const result = checkFieldValue(field, params[field as keyof T], validator);
    if (!result.gain) {
      return result;
    }
  }
  return { gain: true, reason: 'ok', field: 'none' };
}

function checkFieldValue(
  field: string,
  value: unknown,
  schema: ZodSchema
): CheckFieldResult {
  const result = schema.safeParse(value);
  if (result.success === false) {
    const issue = result.error.issues[0];
    return { gain: false, reason: `${field}_${issue.code}`, field };
  } else {
    return { gain: true, reason: 'ok', field };
  }
}

export function checkAllowedField<T extends Record<string, any>>(
  allowedFields: string[],
  doc: T
): boolean {
  for (const field of Object.keys(doc)) {
    if (!allowedFields.includes(field)) {
      return false;
    }
  }
  return true;
}

export const SEARCH_INDEX_SEP = '|';

export function buildSearchIndex<T extends Record<string, any>>(
  doc: T,
  fields: string[]
): string {
  const fieldValues = [];
  for (const field of Object.keys(doc)) {
    if (fields.includes(field)) {
      fieldValues.push(doc[field as keyof T]);
    }
  }
  return fieldValues.join(SEARCH_INDEX_SEP);
}

export function buildSaveError(code: string, raw?: Error) {
  return { error: { code, raw } };
}

export function sortBy<T>(docs: T[], sortOpt: SortOpt): T[] {
  return docs.sort((docA, docB) => {
    const fieldA = getValueByPath(docA, sortOpt.field);
    const fieldB = getValueByPath(docB, sortOpt.field);
    const orderValue =
      sortOpt.order === undefined || sortOpt.order === 'asc' ? 1 : -1;
    if (isUndefinedOrNull(fieldA) && isUndefinedOrNull(fieldB)) {
      return 0;
    }
    if (isUndefinedOrNull(fieldA)) {
      return 1 * orderValue;
    }
    if (isUndefinedOrNull(fieldB)) {
      return -1 * orderValue;
    }
    if (typeof fieldA === 'string' && typeof fieldB === 'string') {
      return fieldA.localeCompare(fieldB) * orderValue;
    }
    if (typeof fieldA === 'number' && typeof fieldB === 'number') {
      return (fieldA - fieldB) * orderValue;
    }
    if (fieldA instanceof Date && fieldB instanceof Date) {
      return (fieldA.getTime() - fieldB.getTime()) * orderValue;
    }
    return 0;
  });
}

function getValueByPath<T>(doc: T, path: string): any {
  return path
    .split('.')
    .reduce((acc, key) => acc && (acc as Record<string, any>)[key], doc);
}

function isUndefinedOrNull(value: any): boolean {
  return value === undefined || value === null;
}

export function fuzzyCompareString(target?: string, keyword?: string): boolean {
  if (!target || !keyword) {
    return false;
    /* ;
     */
  }
  return target.toLowerCase().includes(keyword.toLowerCase());
}

export function normalizeDoc<T extends Record<string, any>>(doc: T): T {
  const valuesToKeep: { [key in keyof T]?: any } = {};
  let valuesToNormalize: { [key in keyof T]?: any } = {};
  for (const docKey of Object.keys(doc)) {
    const key = docKey as keyof T;
    const value = doc[key] as any;
    if (
      value instanceof Blob ||
      value instanceof Date ||
      value instanceof ArrayBuffer ||
      (value instanceof Array &&
        (value as ArrayBuffer[])[0] instanceof ArrayBuffer)
    ) {
      valuesToKeep[key] = toRaw(value) as any;
    } else {
      valuesToNormalize[key] = value;
    }
  }
  valuesToNormalize = JSON.parse(JSON.stringify(valuesToNormalize));
  return Object.assign(valuesToNormalize, valuesToKeep) as T;
}
