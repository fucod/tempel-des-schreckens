import { vi } from 'vitest';
import { z } from 'zod';
import {
  handleModelError,
  checkRequiredField,
  validateParams,
  checkAllowedField,
  sortBy,
  buildSearchIndex,
  normalizeDoc
} from './action';

describe('handleModelError', () => {
  it('calls the callback with "modify_error" for ModifyError', () => {
    const mockCallback = vi.fn();
    const error = new Error();
    error.name = 'ModifyError';
    handleModelError(error, mockCallback);
    expect(mockCallback).toHaveBeenCalledWith('modify_error', 'ModifyError');
  });
  it('calls the callback with "constraint_error" for ConstraintError', () => {
    const mockCallback = vi.fn();
    const error = new Error();
    error.name = 'ConstraintError';
    handleModelError(error, mockCallback);
    expect(mockCallback).toHaveBeenCalledWith(
      'constraint_error',
      'ConstraintError'
    );
  });
  it('calls the callback with "unknown_error" for an unknown error', () => {
    const mockCallback = vi.fn();
    const error = new Error();
    error.name = 'SomeOtherError';
    handleModelError(error, mockCallback);
    expect(mockCallback).toHaveBeenCalledWith(
      'unknown_error',
      'SomeOtherError'
    );
  });
});

describe('checkRequiredField', () => {
  it('returns true if all required fields are present', () => {
    const doc = { name: 'John', age: 30 };
    const fields: (keyof typeof doc)[] = ['name', 'age'];
    expect(checkRequiredField(fields, doc)).toBe(true);
  });
  it('returns false if a required field is missing', () => {
    const doc = { name: 'John' };
    const fields = ['name', 'age'];
    expect(checkRequiredField(fields as (keyof typeof doc)[], doc)).toBe(false);
  });
});

describe('validateParams', () => {
  it('returns gain true with "ok" reason and "none" field for valid params', () => {
    const params = { name: 'Test' };
    const validationMap = { name: z.string() };
    const result = validateParams(params, validationMap);
    expect(result).toEqual({ gain: true, reason: 'ok', field: 'none' });
  });

  it('returns gain false with a reason and field for invalid params', () => {
    const params = { name: 123 };
    const validationMap = { name: z.string() };
    const result = validateParams(params, validationMap);
    expect(result.gain).toBe(false);
    expect(result.field).toBe('name');
    expect(result.reason).toContain('name_invalid_type');
  });
});

describe('checkAllowedField', () => {
  it('returns true if all required fields are present', () => {
    const doc = { name: 'John', age: 30 };
    const fields = ['name', 'age'];
    expect(checkAllowedField(fields, doc)).toBe(true);
  });
  it('returns false if a required field is missing', () => {
    const doc = { name: 'John', age: 30 };
    const fields = ['age'];
    expect(checkAllowedField(fields, doc)).toBe(false);
  });
});

describe('sortBy', () => {
  describe('(field value is string)', () => {
    it('sorts documents by a given field', () => {
      const docs = [{ name: 'b' }, { name: 'a' }, { name: 'c' }];
      const sorted = sortBy(docs, { field: 'name' });
      expect(sorted.map((doc) => doc.name)).toEqual(['a', 'b', 'c']);
    });
  });
  describe('(field value is Date)', () => {
    it('sorts documents by a given field', () => {
      const docs = [
        { name: 'a', createdAt: Date.parse('2023-02-01') },
        { name: 'b', createdAt: Date.parse('2023-01-01') },
        { name: 'c', createdAt: Date.parse('2023-03-01') }
      ];
      const sorted = sortBy(docs, { field: 'createdAt' });
      expect(sorted.map((doc) => doc.name)).toEqual(['b', 'a', 'c']);
    });
  });
  describe('(when some values are empty)', () => {
    it('sorts documents by a given field', () => {
      const docs = [{ name: undefined }, { name: 'a' }, { name: 'c' }];
      const sorted = sortBy(docs, { field: 'name' });
      expect(sorted.map((doc) => doc.name)).toEqual(['a', 'c', undefined]);
    });
  });
  describe('(order)', () => {
    it('sorts documents by a given field in ascending order', () => {
      const docs = [{ age: 30 }, { age: 20 }, { age: 40 }];
      const sorted = sortBy(docs, { field: 'age', order: 'asc' });
      expect(sorted.map((doc) => doc.age)).toEqual([20, 30, 40]);
    });
    it('sorts documents by a given field in descending order', () => {
      const docs = [{ age: 30 }, { age: 20 }, { age: 40 }];
      const sorted = sortBy(docs, { field: 'age', order: 'desc' });
      expect(sorted.map((doc) => doc.age)).toEqual([40, 30, 20]);
    });
    describe('(when no order is given)', () => {
      it('sorts documents by a given field in ascending order by default', () => {
        const docs = [{ age: 30 }, { age: 20 }, { age: 40 }];
        const sorted = sortBy(docs, { field: 'age' });
        expect(sorted.map((doc) => doc.age)).toEqual([20, 30, 40]);
      });
    });
  });
});

describe('buildSearchIndex', () => {
  it('concatenates field values using SEARCH_INDEX_SEP', () => {
    const doc = {
      name: 'John Doe',
      age: 30,
      occupation: 'Software Engineer'
    };
    const fields = ['name', 'occupation'];
    const expected = 'John Doe|Software Engineer';
    const result = buildSearchIndex(doc, fields);
    expect(result).toBe(expected);
  });

  it('handles empty fields array', () => {
    const doc = {
      name: 'Alice',
      age: 25
    };
    const fields: (keyof typeof doc)[] = [];
    const expected = '';
    const result = buildSearchIndex(doc, fields);
    expect(result).toBe(expected);
  });

  it('ignores fields not in the provided list', () => {
    const doc = {
      name: 'Bob',
      age: 35,
      occupation: 'Data Scientist'
    };
    const fields = ['name'];
    const expected = 'Bob';
    const result = buildSearchIndex(doc, fields);
    expect(result).toBe(expected);
  });
});

describe('normalizeDoc', () => {
  it('keeps instances of Blob, Date, and ArrayBuffer unchanged', () => {
    const blob = new Blob();
    const date = new Date();
    const buffer = new ArrayBuffer(8);
    const doc = {
      blobField: blob,
      dateField: date,
      bufferField: buffer,
      stringField: 'test'
    };
    const normalizedDoc = normalizeDoc(doc);
    expect(normalizedDoc.blobField).toBe(blob);
    expect(normalizedDoc.dateField).toBe(date);
    expect(normalizedDoc.bufferField).toBe(buffer);
  });

  it('normalizes fields that are not instances of Blob, Date, or ArrayBuffer', () => {
    const doc = {
      nestedField: { a: 1 },
      nullField: null,
      undefinedField: undefined,
      stringField: 'test'
    };
    const normalizedDoc = normalizeDoc(doc);
    expect(normalizedDoc).toEqual({
      nestedField: { a: 1 },
      nullField: null,
      stringField: 'test'
    });
    expect('undefinedField' in normalizedDoc).toBe(false);
  });
});
