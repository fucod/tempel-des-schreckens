import { z, ZodSchema } from 'zod';
import * as RandomToken from '@sibevin/random-token';

export const UID_RANDOM_SIZE = 16;
const UID_TS_SIZE = 17;

/**
 * Generate an unique ID with the `YYYYMMDDhhmmssxxxAAAAAAAAAAAAAAAA` format, where `YYYYMMDDhhmmssxxx` is a time string from the current date and time, `AAAAAAAAAAAAAAAA` is the random generated string with mixed-case of alphabets and numbers;
 *
 * @example
 * ```ts
 * import { genUid } from '@base-workspace/feature-model';
 *
 * const uid = genUid(); // "202405130645478654VjxFmjppY7m4RP9"
 * ```
 */
export function genUid(): string {
  const nowDt = new Date();
  const ts = nowDt.toISOString().replace(/[^\d]/g, '');
  return `${ts}${RandomToken.gen({ length: UID_RANDOM_SIZE })}`;
}

export const UidRegex = new RegExp(
  `^\\d{${UID_TS_SIZE}}([A-Za-z0-9]{${UID_RANDOM_SIZE}})$`
);

export const UidSchema: ZodSchema = z.string().regex(UidRegex);
