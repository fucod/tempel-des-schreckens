/**
 * Generate a serial-number with a date time and 8-digit random number
 *
 * @example
 * ```ts
 * import { genSn } from '@base-workspace/feature-model';
 *
 * const sn = genSn(); // { dt: <Date>, number: 82134321 }
 * ```
 */
export function genSn(): { dt: Date; num: number } {
  return {
    dt: new Date(),
    num: Math.floor(10000000 + Math.random() * 90000000)
  };
}

/**
 * Show the serial-number string
 *
 * @example
 * ```ts
 * import { genSn } from '@base-workspace/feature-model';
 *
 * const sn = genSn();
 *
 * const sn = {
 *   dt: new Date('2024-03-24T15:33:41.000Z'),
 *   num: 12345678
 * };
 * const snStr = showSn(sn); // "20240324-1533-41000-12345678"
 * ```
 */
export function showSn(sn: { dt: Date; num: number }): string {
  return `${formatDate(sn.dt)}-${sn.num}`;
}

/**
 * Show the date time string of a serial-number
 *
 * @example
 * ```ts
 * import { genSn } from '@base-workspace/feature-model';
 *
 * const sn = genSn();
 *
 * const dt = new Date('2024-03-24T15:33:41.000Z');
 * const snDtStr = showSnDt(dt); // "20240324-1533-41000"
 * ```
 */
export function showSnDt(dt: Date): string {
  return formatDate(dt);
}

function formatDate(date: Date): string {
  const paddingWithZero = (val: number, zeroCount: number) =>
    val.toString().padStart(zeroCount, '0');
  const year = date.getUTCFullYear();
  const month = paddingWithZero(date.getUTCMonth() + 1, 2);
  const day = paddingWithZero(date.getUTCDate(), 2);
  const hours = paddingWithZero(date.getUTCHours(), 2);
  const minutes = paddingWithZero(date.getUTCMinutes(), 2);
  const seconds = paddingWithZero(date.getUTCSeconds(), 2);
  const milliseconds = paddingWithZero(date.getUTCMilliseconds(), 3);
  return `${year}${month}${day}-${hours}${minutes}-${seconds}${milliseconds}`;
}
