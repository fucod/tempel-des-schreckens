import type { PropType } from 'vue';
import type { IconifyIcon } from '@iconify/vue';

export type SelectionOption = {
  /** The selection value. */
  value: string;

  /** The selection label. */
  label?: string;

  /** The selection icon. */
  icon?: IconifyIcon;

  /** The short selection label for mobile display*/
  labelShort?: string;

  /** The search index for this selection option. It is for fuzzy search of select options. */
  searchIndex?: string;
};

/**
 * The prop definition for selection-related components.
 */
export const SELECTION_PROP = {
  /** The selection options. */
  options: {
    type: Array as PropType<SelectionOption[]>,
    required: true
  },

  /** The current selection value. */
  current: {
    type: String,
    default: undefined
  }
};
