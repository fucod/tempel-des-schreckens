import { numberFormatParams } from './factory'; // Replace with your actual module path
import {
  FORMAT_OPTIONS,
  MAX_FRACTION_DIGIT,
  NEGATIVE_DISPLAY_OPTIONS
} from './def';

describe('numberFormatParams', () => {
  it('should return valid NumberFormatParams', () => {
    const params = numberFormatParams();
    expect(params).toHaveProperty('fractionDigit');
    expect(params.fractionDigit).toBeGreaterThanOrEqual(0);
    expect(params.fractionDigit).toBeLessThanOrEqual(MAX_FRACTION_DIGIT);
    expect(NEGATIVE_DISPLAY_OPTIONS).toContain(params.negativeDisplay);
    expect(FORMAT_OPTIONS).toContain(params.notationFormat);
    expect(params.positiveColor).toMatch(/^#[A-F0-9]{6}$/);
    expect(params.zeroColor).toMatch(/^#[A-F0-9]{6}$/);
    expect(params.negativeColor).toMatch(/^#[A-F0-9]{6}$/);
  });
});
