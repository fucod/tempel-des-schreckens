import { formatNumber, fetchCheckSchema } from './action';
import type { NumberFormatOption } from './def';
import { FORMAT_OPTIONS, NEGATIVE_DISPLAY_OPTIONS } from './def';

describe('formatNumber', () => {
  const TEST_CASES: { opts: NumberFormatOption; results: string[] }[] = [
    {
      opts: {
        negativeDisplay: 'sign',
        notationFormat: 'none',
        fractionDigit: 2
      },
      results: ['-123456.70', '0.00', '12.35']
    },
    {
      opts: {
        negativeDisplay: 'bracket',
        notationFormat: 'none',
        fractionDigit: 2
      },
      results: ['(123456.70)', '0.00 ', '12.35 ']
    },
    {
      opts: {
        negativeDisplay: 'sign',
        notationFormat: 'en-US',
        fractionDigit: 2
      },
      results: ['-123,456.70', '0.00', '12.35']
    },
    {
      opts: {
        negativeDisplay: 'sign',
        notationFormat: 'de-DE',
        fractionDigit: 2
      },
      results: ['-123.456,70', '0,00', '12,35']
    },
    {
      opts: {
        negativeDisplay: 'sign',
        notationFormat: 'fr-FR',
        fractionDigit: 2
      },
      results: ['-123\u202f456,70', '0,00', '12,35']
    },
    {
      opts: {
        negativeDisplay: 'sign',
        notationFormat: 'none',
        fractionDigit: undefined
      },
      results: ['-123456.7', '0', '12.345678']
    },
    {
      opts: {
        negativeDisplay: 'sign',
        notationFormat: 'none',
        fractionDigit: 0
      },
      results: ['-123457', '0', '12']
    },
    {
      opts: {
        negativeDisplay: 'sign',
        notationFormat: 'none',
        fractionDigit: 3
      },
      results: ['-123456.700', '0.000', '12.346']
    }
  ];
  const TEST_NUMS = [-123456.7, 0, 12.345678];
  it('formats the number with given options', () => {
    TEST_CASES.forEach((tc) => {
      TEST_NUMS.forEach((num, index) => {
        const result = formatNumber(num, tc.opts);
        expect(result).toBe(tc.results[index]);
      });
    });
  });
});

describe('fetchCheckSchema', () => {
  it('returns a schema for fractionDigit', () => {
    const schema = fetchCheckSchema('fractionDigit');
    expect(schema).toBeDefined();
    expect(() => schema.parse(5)).not.toThrow();
    expect(() => schema.parse(-1)).toThrow();
    expect(() => schema.parse(1.5)).toThrow();
  });

  it('returns a schema for negativeDisplay', () => {
    const schema = fetchCheckSchema('negativeDisplay');
    expect(schema).toBeDefined();
    NEGATIVE_DISPLAY_OPTIONS.forEach((option) => {
      expect(() => schema.parse(option)).not.toThrow();
    });
    expect(() => schema.parse('invalidOption')).toThrow();
  });

  it('returns a schema for notationFormat', () => {
    const schema = fetchCheckSchema('notationFormat');
    expect(schema).toBeDefined();
    FORMAT_OPTIONS.forEach((format) => {
      expect(() => schema.parse(format)).not.toThrow();
    });
    expect(() => schema.parse('invalidFormat')).toThrow();
  });

  it('returns a schema for color', () => {
    const schema = fetchCheckSchema('color');
    expect(schema).toBeDefined();
    expect(() => schema.parse('#FFFFFF')).not.toThrow();
    expect(() => schema.parse('#ZZZ999')).toThrow();
  });

  it('throws for an invalid field', () => {
    expect(() => fetchCheckSchema('invalidField')).toThrow(
      'invalid number format field'
    );
  });
});
