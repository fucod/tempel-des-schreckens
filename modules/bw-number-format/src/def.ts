const AVAILABLE_FORMAT_LOCALES = ['en-US', 'de-DE', 'fr-FR'];
export const FORMAT_OPTIONS = ['none', ...AVAILABLE_FORMAT_LOCALES] as const;
export const MAX_FRACTION_DIGIT = 10;
export const NEGATIVE_DISPLAY_OPTIONS = ['sign', 'bracket'] as const;
export const COLOR_OPTIONS = ['positive', 'negative', 'zero'] as const;

export type NegativeDisplayOption = (typeof NEGATIVE_DISPLAY_OPTIONS)[number];
export type ColorOption = (typeof COLOR_OPTIONS)[number];

export type NumberFormatColorOption = {
  positive?: string;
  negative?: string;
  zero?: string;
};

export type NumberFormatOption = {
  negativeDisplay: NegativeDisplayOption;
  notationFormat: string;
  fractionDigit?: number;
};
