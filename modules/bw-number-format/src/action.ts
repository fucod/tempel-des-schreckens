import { z, ZodSchema } from 'zod';
import { LocaleActor } from '@base-workspace/bw-locale';
import * as Selection from '@base-workspace/bw-selection';
import type { NumberFormatOption } from './def';
import { FORMAT_OPTIONS, NEGATIVE_DISPLAY_OPTIONS } from './def';
import { ColorSchema } from '@base-workspace/bw-color';

export function formatNumber(value: number, opt: NumberFormatOption): string {
  const shownValue =
    value < 0 && opt.negativeDisplay === 'bracket' ? value * -1 : value;
  let formatted = `${shownValue}`;
  if (opt.notationFormat != 'none') {
    const styleOpt: {
      style: string;
      minimumFractionDigits?: number;
      maximumFractionDigits?: number;
    } = {
      style: 'decimal'
    };
    if (opt.fractionDigit || opt.fractionDigit === 0) {
      styleOpt.minimumFractionDigits = opt.fractionDigit;
      styleOpt.maximumFractionDigits = opt.fractionDigit;
    }
    formatted = new Intl.NumberFormat(opt.notationFormat, styleOpt).format(
      shownValue
    );
  } else if (opt.fractionDigit || opt.fractionDigit === 0) {
    formatted = shownValue.toFixed(opt.fractionDigit);
  }
  if (opt.negativeDisplay === 'bracket') {
    if (value < 0) {
      return `(${formatted})`;
    } else {
      return `${formatted} `;
    }
  } else {
    return formatted;
  }
}

const FORMAT_EXAMPLE_NUMBER = 12345.67;
export function buildNotationFormatOptions(
  la: LocaleActor
): Selection.SelectionOption[] {
  return FORMAT_OPTIONS.map((opt) => {
    if (opt === 'none') {
      return {
        value: 'none',
        label: la.t('.notationFormatOption.none') as string
      };
    } else {
      return {
        value: opt,
        label: formatNumber(FORMAT_EXAMPLE_NUMBER, {
          notationFormat: opt as string,
          negativeDisplay: 'sign'
        })
      };
    }
  });
}

export function buildFractionDigitOptions(
  la: LocaleActor
): Selection.SelectionOption[] {
  return FORMAT_OPTIONS.map((opt) => {
    if (opt === 'none') {
      return {
        value: 'none',
        label: la.t('.notationFormatOption.none') as string
      };
    } else {
      return {
        value: opt,
        label: formatNumber(FORMAT_EXAMPLE_NUMBER, {
          notationFormat: opt as string,
          negativeDisplay: 'sign'
        })
      };
    }
  });
}

export function buildNegativeDisplayOptions(
  la: LocaleActor
): Selection.SelectionOption[] {
  return NEGATIVE_DISPLAY_OPTIONS.map((ndo: string) => {
    return {
      value: ndo,
      label: la.t(`.negativeDisplayOption.${ndo}`) as string
    };
  });
}

export function fetchCheckSchema(field: string): ZodSchema {
  switch (field) {
    case 'fractionDigit':
      return z.number().gte(0).int().optional();
    case 'negativeDisplay':
      return z.enum(NEGATIVE_DISPLAY_OPTIONS);
    case 'notationFormat':
      return z.enum(FORMAT_OPTIONS);
    case 'color':
      return ColorSchema;
    default:
      throw 'invalid number format field';
  }
}
