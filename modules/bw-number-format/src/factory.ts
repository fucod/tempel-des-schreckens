import { faker } from '@faker-js/faker';
import type { NegativeDisplayOption } from './def';
import {
  FORMAT_OPTIONS,
  MAX_FRACTION_DIGIT,
  NEGATIVE_DISPLAY_OPTIONS
} from './def';

type NumberFormatParams = {
  fractionDigit: number;
  negativeDisplay: NegativeDisplayOption;
  notationFormat: string;
  positiveColor?: string;
  zeroColor?: string;
  negativeColor?: string;
};

export function numberFormatParams(): NumberFormatParams {
  const fractionDigit = faker.number.int({ min: 0, max: MAX_FRACTION_DIGIT });
  const negativeDisplay = randomPick([
    ...NEGATIVE_DISPLAY_OPTIONS
  ]) as NegativeDisplayOption;
  const notationFormat = randomPick([...FORMAT_OPTIONS]);
  const positiveColor = faker.color.rgb({ casing: 'upper' });
  const zeroColor = faker.color.rgb({ casing: 'upper' });
  const negativeColor = faker.color.rgb({ casing: 'upper' });
  return {
    fractionDigit,
    negativeDisplay,
    notationFormat,
    positiveColor,
    zeroColor,
    negativeColor
  };
}

function randomPick(arr: string[]): string {
  return arr[faker.number.int({ min: 0, max: arr.length - 1 })];
}
