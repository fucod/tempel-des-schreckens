import type { IconifyIcon } from '@iconify/vue';

export type StatusPanelInfo = {
  kind: string;
  text?: string;
  icon: IconifyIcon;
};

export type LabelInfo = {
  icon?: IconifyIcon;
  text?: string;
  colorName?: string;
};
