const VIDEO_CONSTRAINTS = {
  width: { min: 1280, ideal: 1920, max: 2560 },
  height: { min: 720, ideal: 1080, max: 1440 },
  frameRate: { min: 10, ideal: 10, max: 15 }
};

type CameraFacingMode = 'environment' | 'user';
type CameraStatus = 'unavailable' | 'denied' | 'ready' | 'capturing';

export class CameraService {
  public status: CameraStatus = 'unavailable';
  public available = false;
  public stream: MediaStream | null = null;
  public facingMode: CameraFacingMode = 'environment';
  public error = '';

  constructor(params: { facingMode?: CameraFacingMode } = {}) {
    if (!this.available) {
      this.error = 'unavailable';
    }
    if (CameraService.checkAvailability()) {
      this.available = true;
      this.status = 'ready';
      this.facingMode = params.facingMode || 'environment';
    }
  }

  public async start(): Promise<MediaStream | null> {
    if (!this.available) {
      this.error = 'unavailable';
      return null;
    }
    const constraints = {
      video: Object.assign({ facingMode: this.facingMode }, VIDEO_CONSTRAINTS)
    };
    try {
      this.stream = await navigator.mediaDevices.getUserMedia(constraints);
      this.status = 'capturing';
    } catch (err) {
      this.stream = null;
      this.status = 'denied';
      this.error = err as string;
    }
    return this.stream;
  }

  public stop(): void {
    if (this.stream) {
      this.stream.getVideoTracks().forEach((track) => {
        track.stop();
      });
      this.stream = null;
    }
    this.status = this.available ? 'ready' : 'unavailable';
  }

  public async switchFacing(): Promise<MediaStream | null> {
    this.stop();
    this.facingMode = this.facingMode === 'user' ? 'environment' : 'user';
    return this.start();
  }

  static checkAvailability(): boolean {
    return (
      'mediaDevices' in navigator && 'getUserMedia' in navigator.mediaDevices
    );
  }

  static getConstrains() {
    if (
      'mediaDevices' in navigator &&
      'getSupportedConstraints' in navigator.mediaDevices
    ) {
      return navigator.mediaDevices.getSupportedConstraints();
    } else {
      return null;
    }
  }
}
