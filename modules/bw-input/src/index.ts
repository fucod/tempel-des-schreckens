export { default as AutoCompletionInput } from './components/AutoCompletionInput.vue';
export { default as BooleanSwitchInput } from './components/BooleanSwitchInput.vue';
export { default as CameraCaptureInput } from './components/CameraCaptureInput.vue';
export { default as InPlaceInput } from './components/InPlaceInput.vue';
