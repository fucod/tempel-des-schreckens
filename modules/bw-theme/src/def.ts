export const DEFAULT_THEME = 'white';
export const DEFAULT_DARK_THEME = 'black';
export const THEMES = [
  'white',
  'black',
  'ocean',
  'sakura',
  'forest',
  'antique'
];
export const DARK_THEMES = ['black'];
