import { DEFAULT_THEME, DEFAULT_DARK_THEME, DARK_THEMES } from './def';

export function detectLocalTheme(): string {
  if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
    return DEFAULT_DARK_THEME;
  } else {
    return DEFAULT_THEME;
  }
}

export function isThemeDark(theme: string): boolean {
  return DARK_THEMES.includes(theme);
}
